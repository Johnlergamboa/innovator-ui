import React, { useState } from 'react';
import axios from 'axios';

import './style.css'

import { IProp, IState } from './type';
import { Input, message, Upload, Button, Select, Spin } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import type { UploadChangeParam } from 'antd/es/upload';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';



const industry_array = [
  { "value": "Information Technology (IT)", "label": "Information Technology (IT)" },
  { "value": "Agriculture and Agribusiness", "label": "Agriculture and Agribusiness" },
  { "value": "Aerospace and Defense", "label": "Aerospace and Defense" },
  { "value": "Automotive", "label": "Automotive" },
  { "value": "Banking and Finance", "label": "Banking and Finance" },
  { "value": "Biotechnology", "label": "Biotechnology" },
  { "value": "Chemicals", "label": "Chemicals" },
  { "value": "Construction and Real Estate", "label": "Construction and Real Estate" },
  { "value": "Consumer Electronics", "label": "Consumer Electronics" },
  { "value": "Education", "label": "Education" },
  { "value": "Energy (Oil, Gas, Renewable)", "label": "Energy (Oil, Gas, Renewable)" },
  { "value": "Entertainment and Media", "label": "Entertainment and Media" },
  { "value": "Environmental Services", "label": "Environmental Services" },
  { "value": "Food and Beverage", "label": "Food and Beverage" },
  { "value": "Healthcare and Pharmaceuticals", "label": "Healthcare and Pharmaceuticals" },
  { "value": "Hospitality and Tourism", "label": "Hospitality and Tourism" },
  { "value": "Insurance", "label": "Insurance" },
  { "value": "Manufacturing", "label": "Manufacturing" },
  { "value": "Mining and Metals", "label": "Mining and Metals" },
  { "value": "Non-Profit and Social Services", "label": "Non-Profit and Social Services" },
  { "value": "Retail", "label": "Retail" },
  { "value": "Transportation and Logistics", "label": "Transportation and Logistics" },
  { "value": "Utilities (Water, Electricity, Gas)", "label": "Utilities (Water, Electricity, Gas)" },
  { "value": "E-commerce", "label": "E-commerce" },
  { "value": "Fashion and Apparel", "label": "Fashion and Apparel" },
  { "value": "Government and Public Administration", "label": "Government and Public Administration" },
  { "value": "Legal Services", "label": "Legal Services" },
  { "value": "Marketing and Advertising", "label": "Marketing and Advertising" },
  { "value": "Professional Services (Consulting, Accounting)", "label": "Professional Services (Consulting, Accounting)" },
  { "value": "Sports and Recreation", "label": "Sports and Recreation" },
  { "value": "Telecommunications", "label": "Telecommunications" },
  { "value": "Aerospace", "label": "Aerospace" },
  { "value": "Pharmaceuticals", "label": "Pharmaceuticals" },
  { "value": "Biotechnology", "label": "Biotechnology" },
  { "value": "Computer Hardware and Software", "label": "Computer Hardware and Software" },
  { "value": "Internet and Technology", "label": "Internet and Technology" },
  { "value": "Environmental Services", "label": "Environmental Services" },
  { "value": "Publishing and Printing", "label": "Publishing and Printing" },
  { "value": "Architecture and Design", "label": "Architecture and Design" },
  { "value": "Telecommunications", "label": "Telecommunications" },
  { "value": "Textiles and Apparel", "label": "Textiles and Apparel" },
  { "value": "Aerospace", "label": "Aerospace" },
  { "value": "Chemicals and Materials", "label": "Chemicals and Materials" },
  { "value": "Pharmaceuticals and Healthcare", "label": "Pharmaceuticals and Healthcare" },
  { "value": "Renewable Energy", "label": "Renewable Energy" },
  { "value": "Automotive", "label": "Automotive" },
  { "value": "Electronics", "label": "Electronics" },
  { "value": "Telecommunications", "label": "Telecommunications" },
  { "value": "Consumer Goods", "label": "Consumer Goods" }
]



const getBase64 = (img: RcFile, callback: (url: string) => void) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result as string));
  reader.readAsDataURL(img);
};

const beforeUpload = (file: RcFile) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

function Home(props: IProp) {
  const [file, setFile] = useState<any>();
  const [image, setImage] = useState<string>()
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState<string>();
  const [state, setState] = useState<IState>({
    job_title: '',
    job_industry: '',
    ai_response: {
      job_posting_description: '',
      job_responsibilities: []
    },
    fetchingaiLoad: false,
    job_location: '',
    job_salary_range: '',
    about_the_company: '',
    about_company: '',
    temp_Form: {
      job_title: '',
      job_industry: '',
      job_location: '',
      job_salary_range: '',
      about_the_company: '',
      eoe: ''
    },
    imgPreview: '',
    eoe: ''
  })


  const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
    console.log("%c Line:114 🌽 info.file.status", "color:#3f7cff", info.file.status);
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj as RcFile, (url) => {
        setLoading(false);
        setImageUrl(url);
      });
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );


  const requestDescription = async () => {
    const { job_title, job_industry } = state.temp_Form
    const body = {
      job_title,
      job_industry
    }
    setState((prevState) => ({ ...prevState, fetchingaiLoad: true }))
    const response = await axios.post('http://localhost:3000/innovator/jobDescriptionAI', body)
    if (response.data) {
      const { job_posting_description, job_responsibilities } = JSON.parse(response.data[0].message.content);
      setState((prevState) => ({
        ...prevState, ai_response: {
          job_posting_description,
          job_responsibilities
        },
        fetchingaiLoad: false,
        job_title: state.temp_Form.job_title,
        job_industry: state.temp_Form.job_industry,
        job_location: state.temp_Form.job_location,
        job_salary_range: state.temp_Form.job_salary_range,
        about_the_company: state.temp_Form.about_the_company,
        imgPreview: image,
        eoe: state.temp_Form.eoe
      }))
    }
  }


  const handleInput = (event: any) => {
    const { name, value } = event.target;
    console.log("%c Line:173 🍆 value", "color:#fca650", value);
    console.log("%c Line:173 🌰 name", "color:#4fff4B", name);
    const { temp_Form } = state
    setState((prevState) => ({ ...prevState, temp_Form: { ...temp_Form, [name]: value } }))
  }

  const handleInputSelect = (value: string) => {
    console.log("%c Line:189 🍧 value", "color:#2eafb0", value);
    setState((prevState) => ({ ...prevState, temp_Form: {...state.temp_Form, job_industry: value }}))
  }


  const handleImage = async (event: any) => {
    const { name, value, files } = event.target;
    console.log("%c Line:188 🍻 files", "color:#6ec1c2", files);
    setFile(files[0])
  }

  const handleImageUpload = async () => {
    const formData = new FormData();
    formData.append('image', file);

    const response = await axios.post("http://localhost:3000/innovator/file", formData)
    console.log("%c Line:197 🍑 response", "color:#33a5ff", response);
    if (response.data) {
      setImage(response.data.image)
    }
  }


  return (
    <div className="container">
      <div className='basicForm'>
        <div style={{ width: "30%" }}>
          {image ? <img src={`http://localhost:3000/${image}`} alt="avatar" style={{ width: '100px', height: '100px' }} /> : null}
          <input type="file" name='file' onChange={handleImage} />
          <button onClick={handleImageUpload}>Upload</button>
        </div>
        <br />
        <div>
          <span style={{ fontWeight: 'bold' }}>Position Title</span>
          <Input name='job_title' onChange={handleInput} />
        </div>
        <div>
          <span style={{ fontWeight: 'bold' }}>Industry</span>
          <br />
          {/* <Input name='job_industry' onChange={handleInput}/> */}
          <Select
            onChange={handleInputSelect}
            style={{ width: 200 }}
            options={industry_array}
          />
        </div>
        <div>
          <span style={{ fontWeight: 'bold' }}>Location</span>
          <Input name="job_location" onChange={handleInput} />
        </div>
        <div>
          <span style={{ fontWeight: 'bold' }}>Salary Range</span>
          <Input name="job_salary_range" onChange={handleInput} />
        </div>
        <div>
          <span style={{ fontWeight: 'bold' }}>About the Company</span>
          <Input.TextArea style={{ height: "200px" }} name="about_the_company" onChange={handleInput} />
        </div>
        <br />
        <div>
          <span style={{ fontWeight: 'bold' }}>EOE (Optional)</span>
          <Input.TextArea style={{ height: "200px" }} name="eoe" onChange={handleInput} />
        </div>
        <br />
        <div>
          <Button disabled={state.fetchingaiLoad ? true : false} type='primary' onClick={requestDescription}>Generate</Button>
        </div>
      </div>
      <div style={{ border: "1px solid black" }}></div>
      <div className='basicOutput'>
        <div>
          <h1>Preview</h1>
          <div>
            {state.fetchingaiLoad ? (<div style={{flexDirection: 'column', display: 'flex',alignItems: 'center'}}><Spin size="large" /><p style={{fontStyle: 'italic'}}>Generating... Please wait...</p></div>) : null}
            <div>
              {state.imgPreview ? <img src={`http://localhost:3000/${state.imgPreview}`} /> : null}
              <h3>{state.job_title}</h3>
              {state.job_location ? <h3>Location: {state.job_location}</h3> : null}
              <div>
                {state.job_salary_range ? (<h4>Salary Range : ${state.job_salary_range}</h4>) : null}
              </div>
              <div>
                {/* <h3>About The Company: </h3>
                <p>{state.about_the_company}</p> */}
                {state.about_the_company ? (
                   <><h3>About The Company: </h3><p>{state.about_the_company}</p></>
                ): null}
              </div>
              {/* <h4>Job Description</h4>
              {state.ai_response?.job_posting_description ? <p>{state.ai_response.job_posting_description}</p> : null} */}
              {state.ai_response?.job_posting_description ?(
                <>
                <h4>Job Description</h4>
                <p>{state.ai_response.job_posting_description}</p>
                </>
              ) : null}
            </div>
            <div>
              {state.ai_response?.job_responsibilities?.length ? <h4>Job Skills Required</h4> : null}
              <ul>
                {
                  state.ai_response?.job_responsibilities?.length ? state.ai_response?.job_responsibilities.map((items: any) => {
                    return (
                      <li>{items}</li>
                    )
                  }) : null
                }
              </ul>

              {state.ai_response?.job_responsibilities?.length ? <p>{state.ai_response.job_responsibilities}</p> : null}

            </div>
            {
              state.eoe ? (
                <div>
                  <h4>EOE</h4>
                  <p style={{fontStyle: 'italic'}}>{state.eoe}</p>
                </div>
               ) : null
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;