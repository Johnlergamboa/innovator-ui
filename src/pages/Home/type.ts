export interface IProp {}



export interface IState {
    job_title?: string;
    job_industry?: string;
    job_location?: string;
    job_salary_range?: string;
    about_company?: string;
    eoe?: string;
    ai_response?: IAIResponse;
    fetchingaiLoad?: boolean;
    about_the_company?: string;
    temp_Form: ITempForm;
    imgPreview?: string;
}


export interface IAIResponse {
    job_posting_description?: string;
    job_responsibilities?: Array<string>
}


export interface ITempForm {
    job_title: string
    job_industry: string
    job_location: string
    job_salary_range: string
    about_the_company: string
    eoe: string
}