import React from 'react';

import { createBrowserRouter } from 'react-router-dom'
import { Home } from '../pages';


export const root = createBrowserRouter([
    {
        path: '/',
        element: <Home />
    },
    {
        path: "*",
        element: <h1>404 Page Not Found!</h1>
    }
])